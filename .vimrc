call plug#begin('~/.vim/plugged')

" Make sure you use single quotes
Plug 'scrooloose/nerdtree'
Plug 'ryanoasis/vim-devicons'
Plug 'ajh17/VimCompletesMe'
Plug 'vim-syntastic/syntastic'

" Initialize plugin system
call plug#end()

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd FileType vim let b:vcm_tab_complete = 'vim'

set term=builtin_ansi
syntax on
set number
set ruler